<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Otrohy.com.ua</title>
		<meta name="description" content="Otrohy.com.ua" />
		<meta name="keywords" content="Otrohy.com.ua" />

		<link rel="stylesheet" type="text/css" media="screen,projection" href="../css/reset.css" />
		<link rel="stylesheet" type="text/css" media="screen,projection" href="../css/jquery.fancybox.css" />
		<link rel="stylesheet" type="text/css" media="screen,projection" href="../css/global.css" />
		<link rel="stylesheet" type="text/css" media="screen,projection" href="../css/main.css" />
		<link rel="stylesheet" type="text/css" media="screen,projection" href="../css/nivo_slider.css" />

		<!--[if IE 7]>
		<link rel="stylesheet" type="text/css" media="all" href="../css/ie7.css"/>
		<![endif]-->

		<!--[if IE 8]>
		<link rel="stylesheet" type="text/css" media="all" href="../css/ie8.css"/>
		<![endif]-->

		<link href='http://fonts.googleapis.com/css?family=PT+Serif&subset=latin,cyrillic' rel='stylesheet' type='text/css' />

		<script type="text/javascript" src="../js/jquery_1.7.2.min.js"></script>
		<script type="text/javascript" src="http://j.maxmind.com/app/geoip.js"></script>
		<script type="text/javascript" src="../js/jquery.fancybox.pack.js"></script>
		<script type="text/javascript" src="../js/jquery.grayscale.js"></script>
		<script type="text/javascript" src="../js/init.js"></script>
		<link href="../images/favicon.png" rel="icon" type="image/x-icon">

		<script type="text/javascript">
			
			$(document).ready(function() {
				$(".fancybox").fancybox({
					padding: '2'
				});
			});
			
			WebFontConfig = {
				google: { families: [ 'PT+Serif::latin,cyrillic' ] }
			};
			(function() {
				var wf = document.createElement('script');
				wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
					'://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
				wf.type = 'text/javascript';
				wf.async = 'true';
				var s = document.getElementsByTagName('script')[0];
				s.parentNode.insertBefore(wf, s);
			})();
 
		</script>

	</head>
	<body class="gallery-page gallery-page_5">

		<!-- Wrapper Start -->
		<div id="wrapper">

			<!-- Container Start -->
			<div class="container">

				<!-- Menu Start -->
				<div id="menu">
					<ul>
						<li>
							<a href="/inner/inner_1.php">Котеджі</a>
							<!--<div class="submenu">
						  <div class="submenu-bottom">
							  <div class="submenu-midd">
								  <div><a href="#">Додаткові умови</a></div>
								  <div><a href="#">Час заселення</a></div>
							  </div>
						  </div>
					  </div>-->
						</li>
						<li><a href="/inner/inner_2.php">Харчування</a></li>
						<li><a href="/inner/inner_3.php">Відпочинок</a></li>
						<li class="logo"><a href="/" title="Otrohy.com.ua">Otrohy.com.ua</a></li>
						<li><a href="/inner/inner_4.php">Природа</a></li>
						<li><a href="/inner/inner_5.php">Територія</a></li>
						<li><a href="/contacts.php">Контакти</a></li>
					</ul>
					<div class="social-block">
                    			    <a href="https://www.instagram.com/otrohy_resort" target="_blank" class="social-instagram">
                    			        <img src="/images/social-instagram.png" />
                    			    </a>
                    			    <a href="https://www.facebook.com/Otrohy.Resort" target="_blank" class="social-facebook">
                    			        <img src="/images/social-facebook.png" />
                    			    </a>
                    			</div>
				</div>
				<!-- Menu End -->

				<!-- Body Start -->
				<div id="body">

					<div class="article-on-main">
						<div>
							<h1 class="g-title">Галерея <span>Територія</span></h1>
						</div>
						<div>
							<p>Говоріть так, щоб словам було тісно, а думкам просторо</p>
						</div>
					</div>

					<div class="content-block">
						<div class="gallery">
							<ul>
								<li><a href="../images/gallery/territory/img_1_big.jpg" class="fancybox" rel="group_1"><img src="../images/gallery/territory/img_1.jpg" alt="" /></a></li>
								<li><a href="../images/gallery/territory/img_2_big.jpg" class="fancybox" rel="group_1"><img src="../images/gallery/territory/img_2.jpg" alt="" /></a></li>
								<li><a href="../images/gallery/territory/img_3_big.jpg" class="fancybox" rel="group_1"><img src="../images/gallery/territory/img_3.jpg" alt="" /></a></li>
								<li><a href="../images/gallery/territory/img_4_big.jpg" class="fancybox" rel="group_1"><img src="../images/gallery/territory/img_4.jpg" alt="" /></a></li>
								<li><a href="../images/gallery/territory/img_5_big.jpg" class="fancybox" rel="group_1"><img src="../images/gallery/territory/img_5.jpg" alt="" /></a></li>
								<li><a href="../images/gallery/territory/img_6_big.jpg" class="fancybox" rel="group_1"><img src="../images/gallery/territory/img_6.jpg" alt="" /></a></li>
								<li><a href="../images/gallery/territory/img_7_big.jpg" class="fancybox" rel="group_1"><img src="../images/gallery/territory/img_7.jpg" alt="" /></a></li>
								<li><a href="../images/gallery/territory/img_8_big.jpg" class="fancybox" rel="group_1"><img src="../images/gallery/territory/img_8.jpg" alt="" /></a></li>
								<li><a href="../images/gallery/territory/img_9_big.jpg" class="fancybox" rel="group_1"><img src="../images/gallery/territory/img_9.jpg" alt="" /></a></li>
								<li><a href="../images/gallery/territory/img_10_big.jpg" class="fancybox" rel="group_1"><img src="../images/gallery/territory/img_10.jpg" alt="" /></a></li>
								<li><a href="../images/gallery/territory/img_11_big.jpg" class="fancybox" rel="group_1"><img src="../images/gallery/territory/img_11.jpg" alt="" /></a></li>
								<li><a href="../images/gallery/territory/img_12_big.jpg" class="fancybox" rel="group_1"><img src="../images/gallery/territory/img_12.jpg" alt="" /></a></li>
								<li><a href="../images/gallery/territory/img_13_big.jpg" class="fancybox" rel="group_1"><img src="../images/gallery/territory/img_13.jpg" alt="" /></a></li>
								<li><a href="../images/gallery/territory/img_14_big.jpg" class="fancybox" rel="group_1"><img src="../images/gallery/territory/img_14.jpg" alt="" /></a></li>
								<li><a href="../images/gallery/territory/img_15_big.jpg" class="fancybox" rel="group_1"><img src="../images/gallery/territory/img_15.jpg" alt="" /></a></li>
								<li><a href="../images/gallery/territory/img_16_big.jpg" class="fancybox" rel="group_1"><img src="../images/gallery/territory/img_16.jpg" alt="" /></a></li>
								<li><a href="../images/gallery/territory/img_17_big.jpg" class="fancybox" rel="group_1"><img src="../images/gallery/territory/img_17.jpg" alt="" /></a></li>
								<li><a href="../images/gallery/territory/img_18_big.jpg" class="fancybox" rel="group_1"><img src="../images/gallery/territory/img_18.jpg" alt="" /></a></li>
								<li><a href="../images/gallery/territory/img_19_big.jpg" class="fancybox" rel="group_1"><img src="../images/gallery/territory/img_19.jpg" alt="" /></a></li>
								<li><a href="../images/gallery/territory/img_20_big.jpg" class="fancybox" rel="group_1"><img src="../images/gallery/territory/img_20.jpg" alt="" /></a></li>
								<li><a href="../images/gallery/territory/img_21_big.jpg" class="fancybox" rel="group_1"><img src="../images/gallery/territory/img_21.jpg" alt="" /></a></li>
								<li><a href="../images/gallery/territory/img_22_big.jpg" class="fancybox" rel="group_1"><img src="../images/gallery/territory/img_22.jpg" alt="" /></a></li>
								<li><a href="../images/gallery/territory/img_23_big.jpg" class="fancybox" rel="group_1"><img src="../images/gallery/territory/img_23.jpg" alt="" /></a></li>
								<li><a href="../images/gallery/territory/img_24_big.jpg" class="fancybox" rel="group_1"><img src="../images/gallery/territory/img_24.jpg" alt="" /></a></li>
								<li><a href="../images/gallery/territory/img_25_big.jpg" class="fancybox" rel="group_1"><img src="../images/gallery/territory/img_25.jpg" alt="" /></a></li>
								<li><a href="../images/gallery/territory/img_26_big.jpg" class="fancybox" rel="group_1"><img src="../images/gallery/territory/img_26.jpg" alt="" /></a></li>
								<li><a href="../images/gallery/territory/img_27_big.jpg" class="fancybox" rel="group_1"><img src="../images/gallery/territory/img_27.jpg" alt="" /></a></li>
								<li><a href="../images/gallery/territory/img_28_big.jpg" class="fancybox" rel="group_1"><img src="../images/gallery/territory/img_28.jpg" alt="" /></a></li>
								<li><a href="../images/gallery/territory/img_29_big.jpg" class="fancybox" rel="group_1"><img src="../images/gallery/territory/img_29.jpg" alt="" /></a></li>
								<li><a href="../images/gallery/territory/img_30_big.jpg" class="fancybox" rel="group_1"><img src="../images/gallery/territory/img_30.jpg" alt="" /></a></li>
								<li><a href="../images/gallery/territory/img_31_big.jpg" class="fancybox" rel="group_1"><img src="../images/gallery/territory/img_31.jpg" alt="" /></a></li>
								<li><a href="../images/gallery/territory/img_32_big.jpg" class="fancybox" rel="group_1"><img src="../images/gallery/territory/img_32.jpg" alt="" /></a></li>
								<li><a href="../images/gallery/territory/img_33_big.jpg" class="fancybox" rel="group_1"><img src="../images/gallery/territory/img_33.jpg" alt="" /></a></li>
							</ul>
						</div>
						<div class="sidebar">
							
							<ul class="sidebar-menu">
								<li><a href="/gallery/gallery_1.php">Котеджі</a></li>
								<li><a href="/gallery/gallery_2.php">Харчування</a></li>
								<li><a href="/gallery/gallery_3.php">Відпочинок</a></li>
								<li><a href="/gallery/gallery_4.php">Природа</a></li>
								<li><a href="/gallery/gallery_5.php" class="sidebar-menu-active">Територія</a></li>
							</ul>
							
							<div class="some-links">
								<div>
									<a href="/contacts.php"><span>Знайдіть нас<br />на мапі →</span></a>
								</div>
								<div>
									<a href="http://www.ruraltourism.com.ua/" target="_blank"><span>Наші<br />друзі →</span></a>
								</div>
							</div>
						</div>
					</div>

				</div>
				<!-- Body End -->

			</div>
			<!-- Container End -->

		</div>
		<!-- Wrapper End -->

		<!-- Footer Start -->
		<div id="footer">
			<div class="footer-content">
				<div class="footer-top">
					<div class="footer-adress">
						<span>Наша адреса:</span>с. Отрохи, Козелецький р-н, Чернігівська обл.
					</div>
					<div class="footer-phone">
						<span>Подзвоніть нам:</span>+38 (050) 315 05 52<br/>+38 (050) 441 84 33
						<div><a href="mailto:info@otrohy.com.ua">Написати нам!</a></div>
					</div>
					<ul class="footer-links">
						<li><a href="/inner/inner_1.php">Котеджі</a></li>
						<li><a href="/inner/inner_2.php">Харчування</a></li>
						<li><a href="/inner/inner_3.php">Відпочинок</a></li>
						<li><a href="/inner/inner_4.php">Природа</a></li>
						<li><a href="/inner/inner_5.php">Територія</a></li>
						<li><a href="/contacts.php">Контакти</a></li>
					</ul>

					<div class="copy">© Отрохи <?php echo date("Y"); ?></div>
				</div>
				<div class="footer-bottom">
					<!--<ul class="social-links">
				  <li><a href="#" class="fb">fb</a></li>
				  <li><a href="#" class="twitter">twitter</a></li>
				  <li><a href="#" class="youtube">youtube</a></li>
			  </ul>-->

				</div>
			</div>

		</div>
		<!-- Footer End -->

	</body>
</html>
