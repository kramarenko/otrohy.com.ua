<div id="footer">
	<div class="footer-content">
		<div class="footer-top">
			<div class="footer-adress">
				<span>Наша адреса:</span>с. Отрохи, Козелецький р-н, Чернігівська обл.
			</div>
			<div class="footer-phone">
				<span>Подзвоніть нам:</span><a href="tel:+380985994670">+38 (098) 599 46 70</a><br/><a href="tel:+380972344294">+38 (097) 234 42 94</a><br/><a href="tel:+380504418433">+38 (050) 441 84 33</a>
				<div><a href="mailto:otrohy@gmail.com">Написати нам!</a></div>
			</div>
			<ul class="footer-links">
				<li><a href="/inner/inner_1.php">Котеджі</a></li>
				<li><a href="/inner/inner_2.php">Харчування</a></li>
				<li><a href="/inner/inner_3.php">Відпочинок</a></li>
				<li><a href="/inner/inner_4.php">Природа</a></li>
				<li><a href="/inner/inner_5.php">Територія</a></li>
				<li><a href="/contacts.php">Контакти</a></li>
			</ul>

			<div class="copy">© Отрохи <?php echo date("Y"); ?></div>
		</div>
	</div>
</div>
