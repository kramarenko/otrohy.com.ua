/**
 * User: Kramarenko Konstantin
 * Date: 29.10.12
 * Time: 21:29
 * Description
 */


function init() {
    var myMap = new ymaps.Map('contactMap', {
            center: [50.968199, 30.796959],
            zoom: 8,
            controls: ['zoomControl']
        }),
        /**
         * Создание мультимаршрута.
         * @see http://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/multiRouter.MultiRoute.xml
         */
            multiRoute1 = new ymaps.multiRouter.MultiRoute({
            referencePoints: [
                "Киев",
                "Отрохи"
            ]
        }, {
            /**
             * Макет геообъекта.
             * @see http://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/GeoObject.xml#param-options
             */
            routeStrokeColor: "000088",
            routeActiveStrokeColor: "ff0000",
            pinIconFillColor: "ff0000"
        }),

        multiRoute2 = new ymaps.multiRouter.MultiRoute({
            referencePoints: [
                "Чернигов",
                "Отрохи"
            ]
        }, {
            wayPointFinishIconLayout: ''
            // Отключаем режим панели для балуна.
            //balloonPanelMaxMapArea: 0
        }),

        buttonRoute1 = new ymaps.control.Button({
            data: {
                content: "Первый маршрут"
            },
            options: {
                maxWidth: 300
            }
        }),
        buttonRoute2 = new ymaps.control.Button({
            data: {
                content: "Второй маршрут"
            },
            options: {
                maxWidth: 300
            }
        });


    myMap.controls.add(buttonRoute2);
    myMap.controls.add(buttonRoute1);
    myMap.geoObjects.add(multiRoute1);
    myMap.geoObjects.add(multiRoute2);


    buttonRoute1.events.add('select', function () {
        if (buttonRoute2.isSelected()) {
            buttonRoute2.deselect();
        }
        myMap.setBounds(multiRoute1.getBounds());
    });

    buttonRoute2.events.add('select', function () {
        if (buttonRoute1.isSelected()) {
            buttonRoute1.deselect();
        }
        myMap.setBounds(multiRoute2.getBounds());
    });
}

ymaps.ready(init);