<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title>Otrohy.com.ua</title>
	<meta name="description" content="Otrohy.com.ua"/>
	<meta name="keywords" content="Otrohy.com.ua"/>

	<link rel="stylesheet" type="text/css" media="screen,projection" href="css/reset.css"/>
	<link rel="stylesheet" type="text/css" media="screen,projection" href="css/jquery.fancybox.css"/>
	<link rel="stylesheet" type="text/css" media="screen,projection" href="css/global.css"/>
	<link rel="stylesheet" type="text/css" media="screen,projection" href="css/main.css"/>
	<link rel="stylesheet" type="text/css" media="screen,projection" href="css/nivo_slider.css"/>

	<!--[if IE 7]>
	<link rel="stylesheet" type="text/css" media="all" href="css/ie7.css"/>
	<![endif]-->

	<!--[if IE 8]>
	<link rel="stylesheet" type="text/css" media="all" href="css/ie8.css"/>
	<![endif]-->

	<link href='http://fonts.googleapis.com/css?family=PT+Serif&subset=latin,cyrillic' rel='stylesheet' type='text/css'/>

	<script type="text/javascript" src="js/jquery_1.7.2.min.js"></script>
	<script type="text/javascript" src="http://j.maxmind.com/app/geoip.js"></script>
	<script type="text/javascript" src="js/jquery.fancybox.pack.js"></script>

	<script src="//api-maps.yandex.ru/2.1/?load=package.standard,package.geoObjects&lang=ru-RU" type="text/javascript"></script>

	<script type="text/javascript" src="js/ya_map.js"></script>
	<script type="text/javascript" src="js/init.js"></script>
	<link href="images/favicon.png" rel="icon" type="image/x-icon">

	<script type="text/javascript">

		$(document).ready(function(){
			$(".fancybox").fancybox({
				padding: '2'
			});
		});

		WebFontConfig = {
			google: { families: [ 'PT+Serif::latin,cyrillic' ] }
		};
		(function(){
			var wf = document.createElement('script');
			wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
				'://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
			wf.type = 'text/javascript';
			wf.async = 'true';
			var s = document.getElementsByTagName('script')[0];
			s.parentNode.insertBefore(wf, s);
		})();

	</script>

	<?php include("includes/fb_pixel.php");?>

</head>
<body class="contacts-page">

<!-- Wrapper Start -->
<div id="wrapper">

	<!-- Container Start -->
	<div class="container">

		<!-- Menu Start -->
		<div id="menu">
			<ul>
				<li>
					<a href="/inner/inner_1.php">Котеджі</a>
					<!--<div class="submenu">
					  <div class="submenu-bottom">
						  <div class="submenu-midd">
							  <div><a href="#">Додаткові умови</a></div>
							  <div><a href="#">Час заселення</a></div>
						  </div>
					  </div>
				  </div>-->
				</li>
				<li><a href="/inner/inner_2.php">Харчування</a></li>
				<li><a href="/inner/inner_3.php">Відпочинок</a></li>
				<li class="logo"><a href="/" title="Otrohy.com.ua">Otrohy.com.ua</a></li>
				<li><a href="/inner/inner_4.php">Природа</a></li>
				<li><a href="/inner/inner_5.php">Територія</a></li>
				<li><a href="/contacts.php">Контакти</a></li>
			</ul>
			<div class="social-block">
                                			    <a href="https://www.instagram.com/otrohy_resort" target="_blank" class="social-instagram">
                                			        <img src="/images/social-instagram.png" />
                                			    </a>
                                			    <a href="https://www.facebook.com/Otrohy.Resort" target="_blank" class="social-facebook">
                                			        <img src="/images/social-facebook.png" />
                                			    </a>
                                			</div>
		</div>
		<!-- Menu End -->

		<!-- Body Start -->
		<div id="body">

			<div class="article-on-main">
				<h1 class="g-title">Контактна інформація</h1>
			</div>

			<ul class="contact-info">
				<li>Наша адреса – с. Отрохи, Козелецький р-н, Чернігівська обл.</li>
				<li class="phone-number">
					<div>Подзвоніть нам –</div>
					<div>+38 (050) 315 05 52<br />
						+38 (050) 441 84 33
					</div>
				</li>
				<li>Напишіть нам – <a href="mailto:info@otrohy.com.ua">Написати нам!</a><br/><br/></li>
				<li>Для того щоб дістатися до нашої садиби слід скористатися одним із вказаних на схемі маршрутів.</li>
			</ul>

			<div class="content-block">
				<div class="scheme-map">&nbsp;</div>
				<div class="contact-content">
					<h1 class="g-title contact-title">Яндекс карта</h1>
					<div class="contact-map" id="contactMap"></div>
				</div>

				<div class="contact-form">
					<h1 class="g-title contact-title">Напишіть нам!</h1>
					<form action="#" method="post">
						<div class="row">
							<label for="name">Ваше ім’я:</label>
							<input type="text" id="name"/>
						</div>
						<div class="row">
							<label for="email">Ваше ім’я:</label>
							<input type="text" id="email"/>
						</div>
						<div class="row">
							<label for="text">Ваше ім’я:</label>
							<textarea name="text" id="text" cols="30" rows="10"></textarea>
						</div>
						<div class="row">
							<input type="submit" value="Надіслати"/>
						</div>
					</form>
					<div>
						<p>У широкому розумінні природа — органічний і неорганічний матеріальний світ, Всесвіт, у всій сукупності і зв'язках його форм, що є об'єктом людської діяльності і пізнання, основний об'єкт вивчення науки, включно з тим, що створене діяльністю людини. Саме в такому, найширшому розумінні природа
							вивчається природознавством — сукупністю наук про світ, що ставлять перед собою мету відкриття законів природи. </p>
					</div>
				</div>
			</div>

		</div>
		<!-- Body End -->

	</div>
	<!-- Container End -->

</div>
<!-- Wrapper End -->

<!-- Footer Start -->
<?php include("includes/footer.php");?>
<!-- Footer End -->

</body>
</html>
