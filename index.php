<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title>Otrohy.com.ua</title>
	<meta name="description" content="Otrohy.com.ua"/>
	<meta name="keywords" content="Otrohy.com.ua"/>

	<link rel="stylesheet" type="text/css" media="screen,projection" href="css/reset.css"/>
	<link rel="stylesheet" type="text/css" media="screen,projection" href="css/global.css"/>
	<link rel="stylesheet" type="text/css" media="screen,projection" href="css/main.css"/>
	<link rel="stylesheet" type="text/css" media="screen,projection" href="css/nivo_slider.css"/>

	<!--[if IE 7]>
	<link rel="stylesheet" type="text/css" media="all" href="css/ie7.css"/>
	<![endif]-->

	<!--[if IE 8]>
	<link rel="stylesheet" type="text/css" media="all" href="css/ie8.css"/>
	<![endif]-->

	<link href='http://fonts.googleapis.com/css?family=PT+Serif&subset=latin,cyrillic' rel='stylesheet' type='text/css'/>
	<link href="images/favicon.png" rel="icon" type="image/x-icon">

	<?php include("includes/fb_pixel.php");?>

</head>
<body>

<!-- Wrapper Start -->
<div id="wrapper">

	<!-- Container Start -->
	<div class="container">

		<!-- Menu Start -->
		<div id="menu">
			<ul>
				<li>
					<a href="/inner/inner_1.php">Котеджі</a>
					<!--<div class="submenu">
                          <div class="submenu-bottom">
                              <div class="submenu-midd">
                                  <div><a href="#">Додаткові умови</a></div>
                                  <div><a href="#">Час заселення</a></div>
                              </div>
                          </div>
                      </div>-->
				</li>
				<li><a href="/inner/inner_2.php">Харчування</a></li>
				<li><a href="/inner/inner_3.php">Відпочинок</a></li>
				<li class="logo"><a href="/" title="Otrohy.com.ua">Otrohy.com.ua</a></li>
				<li><a href="/inner/inner_4.php">Природа</a></li>
				<li><a href="/inner/inner_5.php">Територія</a></li>
				<li><a href="/contacts.php">Контакти</a></li>
			</ul>
			<div class="social-block">
			    <a href="https://www.instagram.com/otrohy_resort" target="_blank" class="social-instagram">
			        <img src="/images/social-instagram.png" />
			    </a>
			    <a href="https://www.facebook.com/Otrohy.Resort" target="_blank" class="social-facebook">
			        <img src="/images/social-facebook.png" />
			    </a>
			</div>
		</div>
		<!-- Menu End -->

		<div class="slider-wrapp">
			<div class="slider-content">
				<!--<div class="slider-preload"><img src="images/preloader.gif" alt="" /></div>-->
				<div id="slider">
					<img src="images/slide_new_1.jpg" alt=""/>
					<img src="images/slide_new_2.jpg" alt=""/>
                    <img src="images/slide_new_3.jpg" alt=""/>
					<img src="images/slide_new_4.jpg" alt=""/>
					<img src="images/slide_new_5.jpg" alt=""/>
					<img src="images/slide_new_6.jpg" alt=""/>
				</div>
			</div>
		</div>

		<!-- Body Start -->
		<div id="body" class="main-page">

			<div class="article-on-main">
				<div>
					<h1 class="g-title">Запрошуємо усіх любителів зеленого туризму до Садиби Отрохи!</h1>
					<!--<a href="#" class="read-more-link">Читати докладніше</a><span class="arrow"></span>-->
				</div>
				<div>
					<p>Запрошуємо усіх любителів зеленого туризму до Садиби Отрохи!</p>
					<p>Тут ви знайдете затишок лісу та відчуєте природне різноманіття унікального ландшафтного парку, відпочинете від метушні великого міста, краще пізнаєте навколишній світ; разом із тим cмакуватимете чисте повітря, джерельну воду, та домашні страви з найсвіжіших продуктів.</p>
					<p>Ми пропонуємо вам проживання у зручних сучасних котеджах, та безліч варіацій активного відпочинку на природі, від рибальства й навчання верховій їзді, загартування у лазні та прогулянок у лісі, до гри у футбол та бадмінтон.</p>
					<p>Побачимось!</p>

				</div>
			</div>

			<ul class="info-links">
				<li class="info-item_1"><a class="info-link-active" href="/inner/inner_1.php">А для життя які умови?</a></li>
				<li class="info-item_2"><a class="info-link-active" href="/inner/inner_2.php"">А годуємо як?</a></li>
				<li class="info-item_3"><a class="info-link-active" href="/inner/inner_3.php"">Що на рахунок розваг?</a></li>
				<li class="info-item_4"><a class="info-link-active" href="/inner/inner_4.php"">Яка у нас природа?</a></li>
				<li class="info-item_5"><a class="info-link-active" href="/inner/inner_5.php"">Територія велика?</a></li>
			</ul>

			<div class="weather-block">
				<h3 class="g-title">Погода</h3>
					  <div class="weather-content">
						  <!-- Gismeteo informer START -->
                          <link rel="stylesheet" type="text/css" href="https://s1.gismeteo.ua/static/css/informer2/gs_informerClient.min.css">
                          <div id="gsInformerID-38wbq6l0MbOy8a" class="gsInformer" style="width:290px;height:94px">
                            <div class="gsIContent">
                             <div id="cityLink">
                               <a href="https://www.gismeteo.ua/weather-otrokhy-101285/" target="_blank">Погода в Отрохах</a>
                             </div>
                             <div class="gsLinks">
                               <table>
                                 <tr>
                                   <td>
                                     <div class="leftCol">
                                       <a href="https://www.gismeteo.ua" target="_blank">
                                         <img alt="Gismeteo" title="Gismeteo" src="https://s1.gismeteo.ua/static/images/informer2/logo-mini2.png" align="absmiddle" border="0" />
                                         <span>Gismeteo</span>
                                       </a>
                                     </div>
                                     <div class="rightCol">
                                       <a href="https://www.gismeteo.ua/weather-otrokhy-101285/14-days/" target="_blank">Погода на 2 недели</a>
                                     </div>
                                     </td>
                                  </tr>
                                </table>
                              </div>
                            </div>
                          </div>
                          <script src="https://www.gismeteo.ua/ajax/getInformer/?hash=38wbq6l0MbOy8a" type="text/javascript"></script>
                          <!-- Gismeteo informer END -->
					  </div>
				<div class="some-links" style="float: right;">
					<div>
						<a href="/contacts.php"><span>Знайдіть нас<br/>на мапі →</span></a>
					</div>
					<div>
						<a href="http://www.ruraltourism.com.ua/" target="_blank"><span>Наші<br/>друзі →</span></a>
					</div>
				</div>
			</div>

		</div>
		<!-- Body End -->

	</div>
	<!-- Container End -->

</div>
<!-- Wrapper End -->

<!-- Footer Start -->
<?php include("includes/footer.php");?>
<!-- Footer End -->

	<script type="text/javascript" src="js/jquery_1.7.2.min.js"></script>
	<script type="text/javascript" src="js/jquery.nivo.slider.pack.js"></script>
	<script type="text/javascript" src="js/init.js"></script>

	<script type="text/javascript">
		$(window).load(function(){
			$('#slider').nivoSlider({
				effect:       'fade', // Specify sets like: 'fold,fade,sliceDown'
				//slices: 15, // For slice animations
				animSpeed:    500, // Slide transition speed
				pauseTime:    7000, // How long each slide will show
				controlNav:   true, // 1,2,3... navigation
				directionNav: false,
				afterLoad:    function(){
					$('.slider-content').css('background-image', 'none');
				}
			});
		});

		WebFontConfig = {
			google: { families: [ 'PT+Serif::latin,cyrillic' ] }
		};
		(function(){
			var wf = document.createElement('script');
			wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
				'://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
			wf.type = 'text/javascript';
			wf.async = 'true';
			var s = document.getElementsByTagName('script')[0];
			s.parentNode.insertBefore(wf, s);
		})();

	</script>


</body>
</html>
