<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title>Otrohy.com.ua</title>
	<meta name="description" content="Otrohy.com.ua"/>
	<meta name="keywords" content="Otrohy.com.ua"/>

	<link rel="stylesheet" type="text/css" media="screen,projection" href="../css/reset.css"/>
	<link rel="stylesheet" type="text/css" media="screen,projection" href="../css/global.css"/>
	<link rel="stylesheet" type="text/css" media="screen,projection" href="../css/main.css"/>
	<link rel="stylesheet" type="text/css" media="screen,projection" href="../css/nivo_slider.css"/>

	<!--[if IE 7]>
	<link rel="stylesheet" type="text/css" media="all" href="../css/ie7.css"/>
	<![endif]-->

	<!--[if IE 8]>
	<link rel="stylesheet" type="text/css" media="all" href="../css/ie8.css"/>
	<![endif]-->

	<link href='http://fonts.googleapis.com/css?family=PT+Serif&subset=latin,cyrillic' rel='stylesheet' type='text/css'/>

	<script type="text/javascript" src="../js/jquery_1.7.2.min.js"></script>
	<script type="text/javascript" src="http://j.maxmind.com/app/geoip.js"></script>
	<script type="text/javascript" src="../js/init.js"></script>
	<link href="../images/favicon.png" rel="icon" type="image/x-icon">

	<script type="text/javascript">

		WebFontConfig = {
			google: { families: [ 'PT+Serif::latin,cyrillic' ] }
		};
		(function(){
			var wf = document.createElement('script');
			wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
				'://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
			wf.type = 'text/javascript';
			wf.async = 'true';
			var s = document.getElementsByTagName('script')[0];
			s.parentNode.insertBefore(wf, s);
		})();

	</script>

	<?php include("../includes/fb_pixel.php");?>
</head>
<body class="inner-page">

<!-- Wrapper Start -->
<div id="wrapper">

<!-- Container Start -->
<div class="container">

<!-- Menu Start -->
<div id="menu">
	<ul>
		<li>
			<a href="/inner/inner_1.php">Котеджі</a>
			<!--<div class="submenu">
					  <div class="submenu-bottom">
						  <div class="submenu-midd">
							  <div><a href="#">Додаткові умови</a></div>
							  <div><a href="#">Час заселення</a></div>
						  </div>
					  </div>
				  </div>-->
		</li>
		<li><a href="/inner/inner_2.php">Харчування</a></li>
		<li><a href="/inner/inner_3.php">Відпочинок</a></li>
		<li class="logo"><a href="/" title="Otrohy.com.ua">Otrohy.com.ua</a></li>
		<li><a href="/inner/inner_4.php">Природа</a></li>
		<li><a href="/inner/inner_5.php">Територія</a></li>
		<li><a href="/contacts.php">Контакти</a></li>
	</ul>
	<div class="social-block">
    			    <a href="https://www.instagram.com/otrohy_resort" target="_blank" class="social-instagram">
    			        <img src="/images/social-instagram.png" />
    			    </a>
    			    <a href="https://www.facebook.com/Otrohy.Resort" target="_blank" class="social-facebook">
    			        <img src="/images/social-facebook.png" />
    			    </a>
    			</div>
</div>
<!-- Menu End -->

<!-- Body Start -->
<div id="body" class="no-overflow">

	<ul class="info-links">
		<li class="info-item_1"><a href="/inner/inner_1.php">А для життя які умови?</a></li>
		<li class="info-item_2"><a href="/inner/inner_2.php">А годуємо як?</a></li>
		<li class="info-item_3"><a href="/inner/inner_3.php">Що на рахунок розваг?</a></li>
		<li class="info-item_4"><a href="/inner/inner_4.php">Яка у нас природа?</a></li>
		<li class="info-item_5"><a class="info-link-active" href="/inner/inner_5.php">Територія велика?</a></li>
	</ul>

	<div class="content-block no-overflow">

		<div class="inner-content">

			<h1 class="g-title">Велика територія<span>Є де відпочити</span></h1>

			<!--<ul class="bread-crumbs">
					  <li><a href="/">Головна</a></li>
					  <li><span>Харчування в комплексі</span></li>
				  </ul>-->

			<div class="territory">
				<ul>
					<li class="territory-item_1">
						4
						<div class="tooltip-top">
							<div class="tooltip-bottom">
								<div class="tooltip-midd">Комплекс адміністративних будівель</div>
							</div>
						</div>
					</li>
					<li class="territory-item_2">
						2
						<div class="tooltip-top">
							<div class="tooltip-bottom">
								<div class="tooltip-midd">Хата, їдальня</div>
							</div>
						</div>
					</li>
					<li class="territory-item_3">
						1
						<div class="tooltip-top">
							<div class="tooltip-bottom">
								<div class="tooltip-midd">Альтанка</div>
							</div>
						</div>
					</li>
					<li class="territory-item_3_1">
						1
						<div class="tooltip-top">
							<div class="tooltip-bottom">
								<div class="tooltip-midd">Альтанка</div>
							</div>
						</div>
					</li>
					<li class="territory-item_3_2">
						1
						<div class="tooltip-top">
							<div class="tooltip-bottom">
								<div class="tooltip-midd">Альтанка</div>
							</div>
						</div>
					</li>
					<li class="territory-item_4">
						5
						<div class="tooltip-top">
							<div class="tooltip-bottom">
								<div class="tooltip-midd">Котедж №1</div>
							</div>
						</div>
					</li>
					<li class="territory-item_5">
						6
						<div class="tooltip-top">
							<div class="tooltip-bottom">
								<div class="tooltip-midd">Котедж №2</div>
							</div>
						</div>
					</li>
					<li class="territory-item_6">
						7
						<div class="tooltip-top">
							<div class="tooltip-bottom">
								<div class="tooltip-midd">Котеджі №3 та №4 (дуплекс)</div>
							</div>
						</div>
					</li>
					<li class="territory-item_7">
						8
						<div class="tooltip-top">
							<div class="tooltip-bottom">
								<div class="tooltip-midd">Гральний майданчик</div>
							</div>
						</div>
					</li>
					<li class="territory-item_8">
						3
						<div class="tooltip-top">
							<div class="tooltip-bottom">
								<div class="tooltip-midd">Паркінг</div>
							</div>
						</div>
					</li>
					<!--<li class="territory-item_9">
						9
						<div class="tooltip-top">
							<div class="tooltip-bottom">
								<div class="tooltip-midd">Комплекс адміністративних будівель</div>
							</div>
						</div>
					</li>-->
					<li class="territory-item_11">
						10
						<div class="tooltip-top">
							<div class="tooltip-bottom">
								<div class="tooltip-midd">Лазня</div>
							</div>
						</div>
					</li>
					<li class="territory-item_12">
						11
						<div class="tooltip-top">
							<div class="tooltip-bottom">
								<div class="tooltip-midd">Пляж</div>
							</div>
						</div>
					</li>
					<li class="territory-item_13">
						9
						<div class="tooltip-top">
							<div class="tooltip-bottom">
								<div class="tooltip-midd">Місце для медітації</div>
							</div>
						</div>
					</li>
					<li class="territory-item_14">
						12
						<div class="tooltip-top">
							<div class="tooltip-bottom">
								<div class="tooltip-midd">Затишна лава</div>
							</div>
						</div>
					</li>
				</ul>
			</div>

		</div>
		<div class="sidebar">

			<div class="sb-inner-img_5">&nbsp;</div>

			<div class="sb-inner-block">

				<div class="gallery-link"><a href="/gallery/gallery_5.php">Переглянути галерею →</a></div>

				<!--<div class="sb-more-links">
						  <h5>Читайте також</h5>
						  <ul>
							  <li><a href="#">Додаткові можливості</a></li>
							  <li><a href="#">Оренда спорядження</a></li>
							  <li><a href="#">Комплексні заходи</a></li>
						  </ul>
					  </div>-->

				<div class="some-links">
					<div>
						<a href="/contacts.php"><span>Знайдіть нас<br/>на мапі →</span></a>
					</div>
					<div>
						<a href="#"><span>Наші<br/>друзі →</span></a>
					</div>
				</div>
			</div>

		</div>
		<div style="clear:both;"></div>

	</div>


</div>
<!-- Body End -->

</div>
<!-- Container End -->

</div>
<!-- Wrapper End -->

<!-- Footer Start -->
<?php include("../includes/footer.php");?>
<!-- Footer End -->

</body>
</html>
