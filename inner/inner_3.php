<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title>Otrohy.com.ua</title>
	<meta name="description" content="Otrohy.com.ua"/>
	<meta name="keywords" content="Otrohy.com.ua"/>

	<link rel="stylesheet" type="text/css" media="screen,projection" href="../css/reset.css"/>
	<link rel="stylesheet" type="text/css" media="screen,projection" href="../css/global.css"/>
	<link rel="stylesheet" type="text/css" media="screen,projection" href="../css/main.css"/>
	<link rel="stylesheet" type="text/css" media="screen,projection" href="../css/nivo_slider.css"/>

	<!--[if IE 7]>
	<link rel="stylesheet" type="text/css" media="all" href="../css/ie7.css"/>
	<![endif]-->

	<!--[if IE 8]>
	<link rel="stylesheet" type="text/css" media="all" href="../css/ie8.css"/>
	<![endif]-->

	<link href='http://fonts.googleapis.com/css?family=PT+Serif&subset=latin,cyrillic' rel='stylesheet' type='text/css'/>

	<script type="text/javascript" src="../js/jquery_1.7.2.min.js"></script>
	<script type="text/javascript" src="http://j.maxmind.com/app/geoip.js"></script>
	<script type="text/javascript" src="../js/init.js"></script>
	<link href="../images/favicon.png" rel="icon" type="image/x-icon">

	<script type="text/javascript">

		WebFontConfig = {
			google: { families: [ 'PT+Serif::latin,cyrillic' ] }
		};
		(function(){
			var wf = document.createElement('script');
			wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
				'://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
			wf.type = 'text/javascript';
			wf.async = 'true';
			var s = document.getElementsByTagName('script')[0];
			s.parentNode.insertBefore(wf, s);
		})();

	</script>

	<?php include("../includes/fb_pixel.php");?>
</head>
<body class="inner-page">

<!-- Wrapper Start -->
<div id="wrapper">

	<!-- Container Start -->
	<div class="container">

		<!-- Menu Start -->
		<div id="menu">
			<ul>
				<li>
					<a href="/inner/inner_1.php">Котеджі</a>
					<!--<div class="submenu">
						<div class="submenu-bottom">
							<div class="submenu-midd">
								<div><a href="#">Додаткові умови</a></div>
								<div><a href="#">Час заселення</a></div>
							</div>
						</div>
					</div>-->
				</li>
				<li><a href="/inner/inner_2.php">Харчування</a></li>
				<li><a href="/inner/inner_3.php">Відпочинок</a></li>
				<li class="logo"><a href="/" title="Otrohy.com.ua">Otrohy.com.ua</a></li>
				<li><a href="/inner/inner_4.php">Природа</a></li>
				<li><a href="/inner/inner_5.php">Територія</a></li>
				<li><a href="/contacts.php">Контакти</a></li>
			</ul>
			<div class="social-block">
            			    <a href="https://www.instagram.com/otrohy_resort" target="_blank" class="social-instagram">
            			        <img src="/images/social-instagram.png" />
            			    </a>
            			    <a href="https://www.facebook.com/Otrohy.Resort" target="_blank" class="social-facebook">
            			        <img src="/images/social-facebook.png" />
            			    </a>
            			</div>
		</div>
		<!-- Menu End -->

		<!-- Body Start -->
		<div id="body">


			<ul class="info-links">
				<li class="info-item_1"><a href="/inner/inner_1.php">А для життя які умови?</a></li>
				<li class="info-item_2"><a href="/inner/inner_2.php">А годуємо як?</a></li>
				<li class="info-item_3"><a class="info-link-active" href="/inner/inner_3.php">Що на рахунок розваг?</a></li>
				<li class="info-item_4"><a href="/inner/inner_4.php">Яка у нас природа?</a></li>
				<li class="info-item_5"><a href="/inner/inner_5.php">Територія велика?</a></li>
			</ul>

			<div class="content-block">

				<div class="inner-content">

					<h1 class="g-title">Розваги та відпочинок <span>Відпочивай активно!</span></h1>

					<!--<ul class="bread-crumbs">
						<li><a href="/">Головна</a></li>
						<li><span>Харчування в комплексі</span></li>
					</ul>-->

					<ul class="rest-list">
                        <li>Зручна лазня</li>
                        <li>Риболовля на одному з наших ставків або на Десні, що поруч</li>
                        <li>Ексклюзивні екскурсії по ландшафтному парку</li>
                        <li>Навчання верховій їзді</li>
                        <li>Полювання у мисливських угіддях</li>
                        <li>Прогулянки у лісі, в тому числі верхи</li>
                        <li>Восени – по гриби</li>
                        <li>Екскурсії до місцевих пам’яток</li>
                        <li>Спортивні ігри (футбол, бадмінтон, волейбол та інше)</li>
                        <li>Катання на каяках по річці Десна</li>
					</ul>

				</div>
				<div class="sidebar">

					<div class="sb-inner-img_3">&nbsp;</div>

					<div class="sb-inner-block">

						<div class="gallery-link"><a href="/gallery/gallery_3.php">Переглянути галерею →</a></div>

						<!--<div class="sb-more-links">
							<h5>Читайте також</h5>
							<ul>
								<li><a href="#">Додаткові можливості</a></li>
								<li><a href="#">Оренда спорядження</a></li>
								<li><a href="#">Комплексні заходи</a></li>
							</ul>
						</div>-->

						<div class="some-links">
							<div>
								<a href="/contacts.php"><span>Знайдіть нас<br/>на мапі →</span></a>
							</div>
							<div>
								<a href="http://www.ruraltourism.com.ua/" target="_blank"><span>Наші<br />друзі →</span></a>
							</div>
						</div>
					</div>

				</div>

			</div>


		</div>
		<!-- Body End -->

	</div>
	<!-- Container End -->

</div>
<!-- Wrapper End -->

<!-- Footer Start -->
<?php include("../includes/footer.php");?>
<!-- Footer End -->

</body>
</html>
